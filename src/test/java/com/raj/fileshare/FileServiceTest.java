package com.raj.fileshare;

import com.raj.fileshare.dao.FileRepo;
import com.raj.fileshare.dao.UserRepo;
import com.raj.fileshare.entity.File;
import com.raj.fileshare.entity.User;
import com.raj.fileshare.mapper.FileDataMapper;
import com.raj.fileshare.service.FileService;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.InputStream;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class FileServiceTest {

    @InjectMocks
    private FileService fileService;

    @Mock
    private FileRepo fileRepo;

    @Mock
    private UserRepo userRepo;

    @Mock
    private FileDataMapper fileDataMapper;

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void givenFile_WhenUpload_ReturnSuccess() throws Exception {
        final InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream("test-file.text");
        MockMultipartFile multipartFile = new MockMultipartFile("file", "test-file", "text", inputStream);
        Mockito.when(fileRepo.save(Mockito.any())).thenReturn(getFileModel());
        String fileToken = fileService.fileUpload(getUserData(), multipartFile);
        Assert.assertNotNull(fileToken);
    }

    public File getFileModel() {
        File file = new File();
        file.setId(3);
        file.setFileName("file");
        file.setLocation("/home/sys3010/Desktop/files/test-file.text");
        file.setUser(getUserData());
        return file;
    }

    public User getUserData() {
        User user = new User();
        user.setEmail("raj@gmail.com");
        user.setPassword("1234");
        return user;
    }
}

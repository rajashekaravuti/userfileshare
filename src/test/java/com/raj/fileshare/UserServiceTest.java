package com.raj.fileshare;

import com.raj.fileshare.dao.UserRepo;
import com.raj.fileshare.dto.UserDTO;
import com.raj.fileshare.entity.User;
import com.raj.fileshare.mapper.UserDataMapper;
import com.raj.fileshare.service.UserService;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.servlet.http.HttpSession;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@SpringBootTest
@RunWith(SpringJUnit4ClassRunner.class)
public class UserServiceTest {

    @InjectMocks
    private UserService userService;

    @Mock
    private UserRepo userRepo;

    @Mock
    private HttpSession httpSession;

    @Mock
    private UserDataMapper userDataMapper;

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void givenUser_WhenRegistering_ReturnSuccess() throws Exception {
        when(userRepo.findByEmail(anyString())).thenReturn(Optional.of(getUserData()));
        when(userRepo.save(any())).thenReturn(getUserData());
        when(userDataMapper.mapDtoToEntity(any())).thenReturn(getUserData());
        when(userDataMapper.mapEntityToDto(any())).thenReturn(getUserDataDTO());
        UserDTO userDTO = userService.saveUser(getUserDataDTO(), httpSession);
        Assert.assertNotNull(userDTO);
    }

    @Test
    public void givenUser_WhenRegister_ReturnException() throws Exception {
        when(userDataMapper.mapDtoToEntity(any())).thenReturn(getUserWithoutEmail());
        when(userDataMapper.mapEntityToDto(any())).thenReturn(getUserDataDTO());
        when(userRepo.save(any())).thenReturn(getUserData());
        userService.saveUser(getUserDataDTO(), httpSession);
    }

    public User getUserData() {
        User user = new User();
        user.setEmail("raj@gmail.com");
        return user;
    }

    public User getUserWithoutEmail() {
        User user = new User();
        return user;
    }

    public UserDTO getUserDataDTO() {
        UserDTO userDTO = new UserDTO();
        userDTO.setEmail("raj@gmail.com");
        userDTO.setPassword("1234");
        userDTO.setName("raj");
        return userDTO;
    }
}

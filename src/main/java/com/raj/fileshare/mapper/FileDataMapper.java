package com.raj.fileshare.mapper;

import com.raj.fileshare.dto.FileDTO;
import com.raj.fileshare.entity.File;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface FileDataMapper {
    abstract FileDTO mapEntityToDto(File file);

    File mapDtoToEntity(FileDTO fileDTO);
}

package com.raj.fileshare.mapper;

import com.raj.fileshare.dto.UserDTO;
import com.raj.fileshare.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserDataMapper {
    abstract UserDTO mapEntityToDto(User user);

    User mapDtoToEntity(UserDTO userDTO);
}

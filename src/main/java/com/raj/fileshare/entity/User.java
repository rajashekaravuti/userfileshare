package com.raj.fileshare.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Table(indexes = {@Index(name = "email", columnList = "id,email")})
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO) // Making id as Auto increment
    private int id;

    private String name;

    @Column(unique = true) // Making email to unique to get rid of duplicate email's
    private String email;

    private String password;

    private String roles;

    @JsonManagedReference
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    private List<File> files;

    @ToString.Exclude
    @JsonManagedReference
    @ManyToMany(mappedBy = "users", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Share> shares;
}

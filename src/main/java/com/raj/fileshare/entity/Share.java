package com.raj.fileshare.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
public class Share {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO) // Making fid as Auto increment
    private int id;

    @JsonBackReference
    @ManyToMany(cascade = CascadeType.ALL)
    private List<User> users;

    @JsonBackReference
    @ManyToMany(cascade = CascadeType.ALL)
    private List<File> files;
}

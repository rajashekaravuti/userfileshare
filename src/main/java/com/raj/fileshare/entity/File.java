package com.raj.fileshare.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
public class File {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO) // Making fid as Auto increment
    private int id;

    private String fileName;

    private String location;

    @ToString.Exclude
    @JsonBackReference
    @ManyToOne
    private User user;

    @OneToMany(mappedBy = "user")
    private List<File> files;

    @ToString.Exclude
    @JsonManagedReference
    @ManyToMany(mappedBy = "files", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private List<Share> shares;

}

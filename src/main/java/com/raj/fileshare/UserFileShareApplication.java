package com.raj.fileshare;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserFileShareApplication {

	public static void main(String[] args) {
		SpringApplication.run(UserFileShareApplication.class, args);
	}

}

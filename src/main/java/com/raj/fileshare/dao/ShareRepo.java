package com.raj.fileshare.dao;

import com.raj.fileshare.entity.Share;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShareRepo extends JpaRepository<Share, Integer> {
}

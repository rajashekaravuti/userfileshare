package com.raj.fileshare.dto;

import lombok.Data;

@Data
public class UserFileShareDTO {
    private String email;
    private String fileId;
}

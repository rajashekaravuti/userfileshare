package com.raj.fileshare.dto;

import com.raj.fileshare.entity.File;
import com.raj.fileshare.entity.Share;
import com.raj.fileshare.entity.User;
import lombok.Data;

import java.util.List;

@Data
public class FileDTO {
    private int id;
    private String fileName;
    private String location;
    private User user;
    private List<File> files;
    private List<Share> shareModels;
}

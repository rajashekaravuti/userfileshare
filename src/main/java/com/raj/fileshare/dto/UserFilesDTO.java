package com.raj.fileshare.dto;

import com.raj.fileshare.entity.File;
import lombok.Data;

import java.util.List;

@Data
public class UserFilesDTO {
    private List<File> ownedFiles;
    private List<File> sharedFiles;
}

package com.raj.fileshare.service;

import com.raj.fileshare.dao.FileRepo;
import com.raj.fileshare.dao.ShareRepo;
import com.raj.fileshare.dao.UserRepo;
import com.raj.fileshare.dto.UserFileShareDTO;
import com.raj.fileshare.entity.File;
import com.raj.fileshare.entity.Share;
import com.raj.fileshare.entity.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Base64;
import java.util.Collections;
import java.util.Optional;

@Service
@Transactional
public class UserFileShareService {

    Logger logger = LoggerFactory.getLogger(UserFileShareService.class);

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private ShareRepo shareRepo;

    @Autowired
    private FileRepo fileRepo;

    public void userFileShareByFileId(UserFileShareDTO userFileShareDTO, User user) {
        try {
            Optional<User> shareUser = userRepo.findByEmail(userFileShareDTO.getEmail());
            if (shareUser.isPresent()) {
                Optional<File> file = fileRepo.findById(Integer.parseInt(new String(Base64.getDecoder().decode(userFileShareDTO.getFileId()))));
                if (file.isPresent() && file.get().getUser().getId() == user.getId()) {
                    Share share = new Share();
                    share.setUsers(Collections.singletonList(shareUser.get()));
                    share.setFiles(Collections.singletonList(file.get()));
                    shareRepo.save(share);
                }
            }
        } catch (Exception e) {
            logger.error("Exception while sharing file with another user");
        }
    }
}

package com.raj.fileshare.service;

import com.raj.fileshare.dao.FileRepo;
import com.raj.fileshare.dao.UserRepo;
import com.raj.fileshare.dto.FileDTO;
import com.raj.fileshare.dto.UserFilesDTO;
import com.raj.fileshare.entity.File;
import com.raj.fileshare.entity.Share;
import com.raj.fileshare.entity.User;
import com.raj.fileshare.mapper.FileDataMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class FileService {

    Logger logger = LoggerFactory.getLogger(FileService.class);

    @Value("${FILE_RESOURCE_PATH}")
    private String FILE_RESOURCE_PATH;

    @Autowired
    private FileRepo fileRepo;

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private FileDataMapper fileDataMapper;

    public String fileUpload(User user, MultipartFile multipartFile) throws Exception {
        try {
            Path path = Paths.get(FILE_RESOURCE_PATH + new Date().getTime() + "-" + multipartFile.getOriginalFilename());
            Files.write(path, multipartFile.getBytes());
            File file = new File();
            file.setFileName(multipartFile.getOriginalFilename());
            file.setLocation(path.toString());
            file.setUser(user);
            file = fileRepo.save(file);
            if (file.getId() > 0) {
                return Base64.getEncoder().encodeToString(String.valueOf(file.getId()).getBytes());  // Encoding multipartFile token to send and download the multipartFile at another end point
            }
        } catch (Exception e) {
            logger.error("Exception while uploading file : " + e.getMessage());
        }
        throw new Exception();
    }

    public FileDTO downloadUserFile(User user, String fileToken) throws FileNotFoundException {
        try {
            Optional<File> fileModel = fileRepo.findById(Integer.parseInt(new String(Base64.getDecoder().decode(fileToken))));
            if (fileModel.isPresent()) {
                if (fileModel.get().getUser().getId() == user.getId()) {
                    return fileDataMapper.mapEntityToDto(fileModel.get());
                }
                Optional<User> user1 = userRepo.findById(user.getId());
                if (user1.isPresent()) {
                    List<File> files = user1.get().getShares().stream()
                            .flatMap(share -> share.getFiles().stream())
                            .collect(Collectors.toList());
                    List<File> fileList = files.stream()
                            .filter(fileModel1 -> fileModel1.getId() == fileModel.get().getId())
                            .collect(Collectors.toList());
                    if (fileList.size() > 0) {
                        return fileDataMapper.mapEntityToDto(fileModel.get());
                    }
                }
            }
        } catch (Exception e) {
            logger.error("Exception while downloading file : " + e.getMessage());
        }
        throw new FileNotFoundException();
    }

    public UserFilesDTO getUserFiles(User user) throws Exception {
        try {
            Optional<User> userModel1 = userRepo.findById(user.getId());
            if (userModel1.isPresent()) {
                UserFilesDTO userFilesDTO = new UserFilesDTO();
                userFilesDTO.setOwnedFiles(userModel1.get().getFiles());
                userFilesDTO.setSharedFiles(userModel1.get().getShares().stream().flatMap(share -> share.getFiles().stream()).collect(Collectors.toList()));
              return userFilesDTO;
            }
        } catch (Exception e) {
            logger.error("Exception at getUserFiles() in FileService.java : " + e.getMessage());
        }
        throw new Exception();
    }
}

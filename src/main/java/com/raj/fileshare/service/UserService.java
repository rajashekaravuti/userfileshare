package com.raj.fileshare.service;

import com.raj.fileshare.dao.UserRepo;
import com.raj.fileshare.dto.UserDTO;
import com.raj.fileshare.entity.User;
import com.raj.fileshare.mapper.UserDataMapper;
import com.raj.fileshare.security.UserAuth;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;
import java.util.Objects;
import java.util.Optional;

@Service
@Transactional
public class UserService implements UserDetailsService {

    @Autowired
    private UserRepo userRepo;

    @Autowired
    private UserDataMapper userDataMapper;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public UserDTO saveUser(UserDTO userDTO, HttpSession httpSession) throws Exception {
        if (Objects.nonNull(userDTO) && StringUtils.hasText(userDTO.getEmail())) {
            Optional<User> user = userRepo.findByEmail(userDTO.getEmail());
            if (user.isPresent()) {
                httpSession.setAttribute("user", user.get());
                return userDataMapper.mapEntityToDto(user.get());
            } else {
                User user1 = userDataMapper.mapDtoToEntity(userDTO);
                user1.setName(user1.getEmail().split("@")[0]); // getting name from email and adding to name field
                user1.setPassword(passwordEncoder.encode(user1.getPassword()));
                user1.setRoles("USER");
                user1 = userRepo.save(user1);// saving user data into database
                httpSession.setAttribute("user", user1);
                return userDataMapper.mapEntityToDto(user1);
            }
        }
        throw new Exception();
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userRepo.findByEmail(email).orElse(null);
        if (user != null) {
            return new UserAuth(user);
        }
        throw new UsernameNotFoundException("User failed to authenticate.");
    }
}

package com.raj.fileshare.controller;

import com.raj.fileshare.dto.FileDTO;
import com.raj.fileshare.dto.UserFilesDTO;
import com.raj.fileshare.entity.User;
import com.raj.fileshare.security.UserAuth;
import com.raj.fileshare.service.FileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

@RestController
public class FileController {

    Logger logger = LoggerFactory.getLogger(FileController.class);

    @Autowired
    private FileService fileService;

    @PostMapping("api/file")
    public ResponseEntity<String> fileUpload(@RequestParam("file") MultipartFile file) {
        try {
            User user = UserAuth.getAuthUser();
            String fileTokenEncode = fileService.fileUpload(user, file);
            return ResponseEntity.status(HttpStatus.OK).body(fileTokenEncode);
        } catch (Exception e) {
            logger.error("Exception while uploading file : " + e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("api/file")
    @ResponseBody
    public ResponseEntity<UserFilesDTO> getUserFilesInfo() {
        try {
            User user = UserAuth.getAuthUser();
            UserFilesDTO userFilesDTO = fileService.getUserFiles(user);
            return ResponseEntity.status(HttpStatus.OK).body(userFilesDTO);
        } catch (Exception e) {
            logger.error("Exception while getting user files : " + e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("api/file/{filetoken}")
    public ResponseEntity<Resource> downloadUserFile(@PathVariable String filetoken) {
        try {
            User user = UserAuth.getAuthUser();
            FileDTO fileDTO = fileService.downloadUserFile(user, filetoken);
            if (fileDTO != null) {
                File file = new File(fileDTO.getLocation());
                HttpHeaders headers = new HttpHeaders();
                headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
                headers.add("Pragma", "no-cache");
                headers.add("Expires", "0");
                return ResponseEntity.ok()
                        .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getName() + "\"")
                        .contentLength(file.length())
                        .contentType(MediaType.APPLICATION_OCTET_STREAM)
                        .body(new InputStreamResource(new FileInputStream(file)));
            } else {
                logger.error("User access denied for accessing this file");
                return new ResponseEntity<>(HttpStatus.FORBIDDEN);
            }
        } catch (FileNotFoundException e) {
            logger.error("File not found with this user.");
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        } catch (Exception e) {
            logger.error("Exception while downloading file.");
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }
}

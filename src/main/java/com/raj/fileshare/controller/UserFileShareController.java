package com.raj.fileshare.controller;

import com.raj.fileshare.dto.UserFileShareDTO;
import com.raj.fileshare.entity.User;
import com.raj.fileshare.security.UserAuth;
import com.raj.fileshare.service.UserFileShareService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class UserFileShareController {

    Logger logger = LoggerFactory.getLogger(UserFileShareController.class);

    @Autowired
    private UserFileShareService userFileShareService;

    @PostMapping("api/share")
    public ResponseEntity<Resource> userFileShare(@RequestBody UserFileShareDTO userFileShareDTO) {
        try {
            User user = UserAuth.getAuthUser();
            userFileShareService.userFileShareByFileId(userFileShareDTO, user);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception e) {
            logger.error("User don't have permission for this file to share with others");
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

}

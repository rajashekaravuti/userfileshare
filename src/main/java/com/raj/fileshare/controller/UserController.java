/**
 *
 */
package com.raj.fileshare.controller;

import com.raj.fileshare.dto.UserDTO;
import com.raj.fileshare.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpSession;

/**
 * @author Rajashekar Avuti
 *
 */
@RestController
public class UserController {

    Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @PostMapping("register")
    @ResponseBody
    public ResponseEntity<HttpStatus> userRegister(@RequestBody UserDTO userDTO, HttpSession httpSession) {
        try {
            userService.saveUser(userDTO, httpSession);
            return new ResponseEntity<>(HttpStatus.CREATED); // returning response status code 201
        } catch (Exception e) {
            logger.error("Exception at userRegister() in UserController.java : " + e.getMessage());
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

﻿Welcome to userfileshare
-------------------------------
This project was build to share the files between users

**Tech Stack :**

* Java 1.8
* Maven
* Spring Boot
* Spring security
* H2 Database
* JPA
* Restfull
* Lombok

**Service endpoints :**

* POST /register
* POST /api/file
* GET /api/file
* GET /api/file/{filetoken}
* POST /api/share


**Tables :**

* USER
* FILE
* SHARE
* SHARE_FILES
* SHARE_USERS

**Properties :**

* FILE_RESOURCE_PATH=/home/sys3010/Desktop/files/
* spring.datasource.url=jdbc:h2:mem:raj

**Note :** Development system supported properties used in development. Please update when testing (application.properties)
